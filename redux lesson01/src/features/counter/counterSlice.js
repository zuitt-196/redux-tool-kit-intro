import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    count: 0
}

export const counterSlice = createSlice({
    name: 'counter',
    initialState,
    reducers: {
        increment: (state) => {
            state.count += 1;
        },
        decrement: (state) => {
            state.count -= 1;
        },
        reset: (state) => {
            state.count = 0;
        },
        incrementByAmount: (state, action) => {
            state.count += action.payload;
        }
    }
});

export const { increment, decrement, reset, incrementByAmount } = counterSlice.actions;

export default counterSlice.reducer;
























// import { createSlice } from '@reduxjs/toolkit';

// const initialState = {
//     count: 0
// }



// export const counterSlice = createSlice({
//     name: 'counter',
//     initialState,
//     reducer: {
//         // HAS TO ACTION IN REDUCER
//         increment: (state) => {
//             state.count += 1;
//             // IN NORAML EXRESSION IS state.count = state.count + 1;
//         },
//         decrement: (state) => {
//             state.count -= 1;
//             // IN NORAML EXRESSION IS state.count = state.count - 1;
//         }
//     }

// })

// // export the two  action wraped by the  detrtucture object in order to passed the other component
// // with the value of counterSlice.actions
// // actions statement is object that represent payload of information that send data from your application to you store
// // Actyion have (type and an optional payload)
// export const { increment, decrement } = counterSlice.actions;


// // export the  counterSlice with access the reducer object with the value action with
// // although the action was export already but we need to export in order to pass in stor component which is the global store componet
// export default counterSlice.reducer;