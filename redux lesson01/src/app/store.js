import { configureStore } from '@reduxjs/toolkit';

// from the counrterSlice component 
import counterReducer from '../features/counter/counterSlice'

// the logic concept this component -> is to given a data in whole applicaion by used of {Prvider} from reacti-rduxx
// basically it cand access all state in every action of application 
export const store = configureStore({
    reducer: {
        // define the variable counter with value of import counterReducer
        counter: counterReducer,
    }

})